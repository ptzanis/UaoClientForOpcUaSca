/*
 * VariantReaderWriter.cpp
 *
 *  Created on: Sep 7, 2020
 *      Author: pnikiel
 */

#include "VariantReaderWriter.h"

#include <UaoClientForOpcUaScaUaoExceptions.h>

namespace UaoClientForOpcUaSca
{

VariantReaderWriter::VariantReaderWriter(
		UaClientSdk::UaSession* session,
		const UaNodeId&         variableNodeId) :
			m_session(session),
			m_variableNodeId (variableNodeId)
			{

			}

UaVariant VariantReaderWriter::read(
		UaStatus*   out_status,
		UaDateTime* sourceTimeStamp,
		UaDateTime* serverTimeStamp)
{

	ServiceSettings   ss;
	UaReadValueIds    nodesToRead;
	UaDataValues      dataValues;
	UaDiagnosticInfos diagnosticInfos;

	nodesToRead.create(1);
	m_variableNodeId.copyTo( &nodesToRead[0].NodeId );
	nodesToRead[0].AttributeId = OpcUa_Attributes_Value;

	dataValues.create (1);

	UaStatus status = m_session->read(
			ss,
			0 /*max age*/,
			OpcUa_TimestampsToReturn_Both,
			nodesToRead,
			dataValues,
			diagnosticInfos
	);

	if (status.isBad())
		throw std::runtime_error(std::string("OPC-UA read failed:")+status.toString().toUtf8()); // TODO move to more specialized exception type.

	if (out_status)
		*out_status = dataValues[0].StatusCode;
	else
	{
		if (! UaStatus(dataValues[0].StatusCode).isGood())
			throw std::runtime_error(std::string("OPC-UA read: variable status is not good") + UaStatus(dataValues[0].StatusCode).toString().toUtf8() ); // TODO move to more specialized exception type.
	}

	if (sourceTimeStamp)
		*sourceTimeStamp = dataValues[0].SourceTimestamp;
	if (serverTimeStamp)
		*serverTimeStamp = dataValues[0].ServerTimestamp;

	return UaVariant(dataValues[0].Value);

}

void  VariantReaderWriter::write (
	    UaVariant&     data,
	    UaStatus*      out_status)
{
    ServiceSettings   ss;
    UaWriteValues     nodesToWrite;
    UaDataValues      dataValues;
    UaDiagnosticInfos diagnosticInfos;
    UaStatusCodeArray results;

    nodesToWrite.create(1);
    m_variableNodeId.copyTo( &nodesToWrite[0].NodeId );
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;

    dataValues.create (1);
    data.copyTo( &nodesToWrite[0].Value.Value );

    UaStatus status = m_session->write(
                          ss,
                          nodesToWrite,
                          results,
                          diagnosticInfos
                      );
    if (out_status)
    {
        *out_status = status;
    }
    else
    {
        if (status.isBad())
            throw Exceptions::BadStatusCode("OPC-UA write failed", status.statusCode() );
        if (results[0] != OpcUa_Good)
            throw Exceptions::BadStatusCode ("OPC-UA write failed", results[0] );
    }

}

}
