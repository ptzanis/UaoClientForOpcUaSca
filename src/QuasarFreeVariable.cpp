#include <QuasarFreeVariable.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>

namespace UaoClientForOpcUaSca
{
namespace VariantConverters
{

template<> bool convertFromVariant (const UaVariant& variant)
{
	OpcUa_Boolean x;
	if (! variant.toBool(x).isGood())
		throw Exceptions::TypeConversionException(
				std::to_string(variant.type()),
				"bool",
				__PRETTY_FUNCTION__);
	return x;
}

template<>
std::string convertFromVariant (const UaVariant& variant)
{
	return variant.toString().toUtf8();
}

template<>
OpcUa_UInt32 convertFromVariant (const UaVariant& variant)
{
	OpcUa_UInt32 x;
	if (! variant.toUInt32(x).isGood())
		throw Exceptions::TypeConversionException(
				std::to_string(variant.type()),
				"OpcUa_UInt32",
				__PRETTY_FUNCTION__);
	return x;
}

template<>
OpcUa_Int32 convertFromVariant (const UaVariant& variant)
{
	OpcUa_Int32 x;
	if (! variant.toInt32(x).isGood())
		throw Exceptions::TypeConversionException(
				std::to_string(variant.type()),
				"OpcUa_Int32",
				__PRETTY_FUNCTION__);
	return x;
}

template<> OpcUa_Float convertFromVariant (const UaVariant& variant)
{
	OpcUa_Float x;
	if (! variant.toFloat(x).isGood())
		throw Exceptions::TypeConversionException(
				std::to_string(variant.type()),
				"OpcUa_Float",
				__PRETTY_FUNCTION__);
	return x;
}

template<> OpcUa_Double convertFromVariant (const UaVariant& variant)
{
	OpcUa_Double x;
	if (! variant.toDouble(x).isGood())
		throw Exceptions::TypeConversionException(
				std::to_string(variant.type()),
				"OpcUa_Double",
				__PRETTY_FUNCTION__);
	return x;
}

template<> void convertToVariant (UaVariant& variant, bool x)
{
	variant.setBool(x ? OpcUa_True : OpcUa_False);
}

template<>
void convertToVariant (UaVariant& variant, std::string x)
{
	variant.setString(x.c_str());
}

template<>
void convertToVariant (UaVariant& variant, OpcUa_UInt32 x)
{
	variant.setUInt32(x);
}

template<> void convertToVariant (UaVariant& variant, OpcUa_Int32 x)
{
	variant.setInt32(x);
}

template<> void convertToVariant (UaVariant& variant, OpcUa_Float x)
{
	variant.setFloat(x);
}

template<> void convertToVariant (UaVariant& variant, OpcUa_Double x)
{
	variant.setDouble(x);
}

}
}
