

#include <iostream>
#include <GpioBitBanger.h>
#include <uaclient/uasession.h>
#include <stdexcept>
#include <UaoClientForOpcUaScaArrayTools.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>

namespace UaoClientForOpcUaSca
{


GpioBitBanger::
GpioBitBanger
(
    UaSession* session,
    UaNodeId objId
) :
    m_session(session),
    m_objId (objId)
{

}




void GpioBitBanger::bitBang (
    const UaByteString&  in_requestsMessageSerialized,
    std::vector<OpcUa_UInt32>& out_readRegisterData
)
{

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = m_objId;
    callRequest.methodId = UaNodeId( UaString(m_objId.identifierString()) + UaString(".bitBang"), 2 );


    UaVariant v;


    callRequest.inputArguments.create( 1 );

    v.setByteString( const_cast<UaByteString&>(in_requestsMessageSerialized), false );



    v.copyTo( &callRequest.inputArguments[ 0 ] );



    UaStatus status =
        m_session->call(
            serviceSettings,
            callRequest,
            co
        );
    if (status.isBad())
        throw Exceptions::BadStatusCode("In OPC-UA call", status.statusCode());


    v = co.outputArguments[0];

    ArrayTools::convertUaVariantToVector( v, out_readRegisterData );



}



}


