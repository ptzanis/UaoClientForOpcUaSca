/*
 * IoBatch.cpp
 *
 *  Created on: 12 Nov 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 */

#include <IoBatch.h>
#include <uaclient/uasession.h>
#include <stdexcept>
#include <UaoClientForOpcUaScaArrayTools.h>
#include <UaoClientForOpcUaScaUaoExceptions.h>
#include <GpioBitBanger.h>
#include <iostream>
#include <LogIt.h>

#include <ClientSessionFactory.h>

namespace UaoClientForOpcUaSca
{

IoBatch::direction const IoBatch::OUTPUT = ScaPb::BitBangRequest_Direction_OUTPUT;
IoBatch::direction const IoBatch::INPUT = ScaPb::BitBangRequest_Direction_INPUT;

IoBatch::IoBatch( UaSession* session, UaNodeId objId ) :
    m_session(session),
    m_objId (objId)
{

}

IoBatch::~IoBatch()
{

}

void IoBatch::addSetPins( std::map<uint32_t, bool> pinValue, uint32_t delay )
{

    LOG(Log::DBG) << "Adding a set-pins request";

    if ( pinValue.empty() )
        throw std::runtime_error("No pins and no values were passed");

    ScaPb::BitBangRequest* request = m_group.add_requests();

	request->set_type( ScaPb::BitBangRequest_RequestType_WRITE );

    std::for_each (
        pinValue.begin(),
        pinValue.end(),
        [&]( const std::pair<uint32_t, bool>& pinValueEntry )
        {
            request->add_pins( pinValueEntry.first );
	        request->add_values( pinValueEntry.second );
        }
    );

    request->set_trailingpause( delay );

}

void IoBatch::addGetPins( uint32_t delay )
{

    LOG(Log::DBG) << "Adding a get-pins request";

    ScaPb::BitBangRequest* request = m_group.add_requests();

	request->set_type( ScaPb::BitBangRequest_RequestType_READ );

	request->set_trailingpause( delay );

}

void IoBatch::addSetPinsDirections( std::map<uint32_t, direction> pinDirection, uint32_t delay )
{

    LOG(Log::DBG) << "Adding a set-directions request";

    if ( pinDirection.empty() )
        throw std::runtime_error("No pins and no directions were passed");

    ScaPb::BitBangRequest* request = m_group.add_requests();

	request->set_type( ScaPb::BitBangRequest_RequestType_SET_DIRECTION );

    std::for_each (
        pinDirection.begin(),
        pinDirection.end(),
        [&]( const std::pair<uint32_t, direction>& pinDirectionEntry )
        {
            request->add_pins( pinDirectionEntry.first );
	        request->add_directions( pinDirectionEntry.second );
        }
    );

    request->set_trailingpause( delay );

}

std::vector<OpcUa_UInt32> IoBatch::dispatch()
{

    LOG(Log::DBG) << "Dispatching batch of GPIO requests";

    LOG(Log::DBG) << "Bytes to dispatch: " << m_group.ByteSize();

    UaByteString requestsMessageSerialized;

    size_t size = m_group.ByteSize();
    std::unique_ptr<unsigned char[]> serialized(new unsigned char[size]);

    m_group.SerializeToArray( &serialized[0], static_cast<int>(size) );
    
    requestsMessageSerialized.setByteString( static_cast<int>(size), &serialized[0] );

    std::vector<OpcUa_UInt32> interestingReplies;

    GpioBitBanger bitBanger ( m_session, m_objId );

    bitBanger.bitBang( requestsMessageSerialized, interestingReplies );

    clearBatch();

    return interestingReplies;

}

std::vector<bool> repliesToPinBits( const std::vector<uint32_t>& interestingReplies, uint32_t pinOfInterest )
{

    std::vector<bool> interestingPinValues;

    std::transform(
        interestingReplies.begin(),
        interestingReplies.end(),
        std::back_inserter( interestingPinValues ),
        [&pinOfInterest]( const uint32_t& interestingReply )
        {
            return ( ( interestingReply >> pinOfInterest ) & 1);
        }
    );

    return interestingPinValues;

}

}
