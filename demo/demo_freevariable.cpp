/*
 * demo.cpp
 *
 *  Created on: 25 Oct 2017
 *      Author: pnikiel
 *
 */

// the following file is provided with UaoForQuasar
#include <ClientSessionFactory.h>

// the following comes from generation
// you might need to use another class
#include <QuasarFreeVariable.h>

#include <uaplatformlayer.h>

#include <LogIt.h>

using namespace UaoClientForOpcUaSca;

int main()
{
    UaPlatformLayer::init();
    Log::initializeLogging(Log::INF);
    try
    {
    	UaClientSdk::UaSession* session = ClientSessionFactory::tryConnect("opc.tcp://127.0.0.1:4841");
    	QuasarFreeVariable<std::string> fv_string (session, UaNodeId("fv_string", 2));
    	LOG(Log::INF) << "Value read using FreeVariable: " << fv_string.read();
    	fv_string.write("by testing you will find the solution");
    	delete session;
    }
    catch (const std::exception &e)
    {
        LOG(Log::ERR) << "Caught: " << e.what();
    }
}




