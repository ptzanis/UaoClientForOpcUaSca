# Authors: Paris Moschovakos (paris.moschovakos@cern.ch)
# Date: 13/08/2020
#
# Description: This is script is part of the CI/CD of UAoClientForOpcUaSca
# It's purpose is to create a package of the demos as built by the CI of this
# repository. The tools are delivered to the users for debugging and
# demonstration purposes

echo "Package demo programs"
DIR=ScaClientTools-$1
rm -Rf $DIR $DIR".tar.gz" 
mkdir $DIR 
cp demo/build/bitBangI2c demo/build/demo demo/build/demo_adc_samples demo/build/demo_bitBanger demo/build/demo_send_bitfile $DIR 
tar cf $DIR".tar" $DIR 
gzip -9 $DIR".tar" 
