
#ifndef __UAO__UaoClientForOpcUaSca__SpiSystem__
#define __UAO__UaoClientForOpcUaSca__SpiSystem__

#include <iostream>
#include <uaclient/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;



class SpiSystem
{

public:

    SpiSystem(
        UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables


private:

    UaSession  * m_session;
    UaNodeId     m_objId;

};



}

#endif // __UAO__UaoClientForOpcUaSca__SpiSystem__