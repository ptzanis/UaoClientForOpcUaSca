/*
 * IoBatch.h
 *
 *  Created on: 12 Nov 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 */

#pragma once

#include <uaclient/uaclientsdk.h>
#include <BitBangProtocol.pb.h>
#include <memory>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;

std::vector<bool> repliesToPinBits( const std::vector<uint32_t>& interestingReplies, uint32_t pinOfInterest );

class IoBatch
{
public:

    IoBatch( UaSession* session, UaNodeId objId );
    ~IoBatch();

    typedef ScaPb::BitBangRequest_Direction direction;
    static direction const OUTPUT;
    static direction const INPUT;

    void addSetPins( std::map<uint32_t, bool> pinValue, uint32_t delay = 0 );
    void addGetPins( uint32_t delay = 0 );
    void addSetPinsDirections( std::map<uint32_t, direction> pinDirection, uint32_t delay = 0 );
    std::vector<OpcUa_UInt32> dispatch();

    int getCurrentBatchSize() { return m_group.requests_size(); }
    void clearBatch() { m_group.Clear(); }

private:
    ScaPb::BitBangRequests m_group;
    UaSession  * m_session;
    UaNodeId     m_objId;

};

}
