#pragma once

#include <uaclient/uaclientsdk.h>

#include <VariantReaderWriter.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;

namespace VariantConverters
{
template<typename T>
T convertFromVariant (const UaVariant& variant);

template<> bool convertFromVariant (const UaVariant& variant);
template<> std::string convertFromVariant (const UaVariant& variant);
template<> OpcUa_UInt32 convertFromVariant (const UaVariant& variant);
template<> OpcUa_Int32 convertFromVariant (const UaVariant& variant);
template<> OpcUa_Float convertFromVariant (const UaVariant& variant);
template<> OpcUa_Double convertFromVariant (const UaVariant& variant);

template<typename T>
void convertToVariant (UaVariant& variant, T x);

template<> void convertToVariant (UaVariant& variant, bool x);
template<> void convertToVariant (UaVariant& variant, std::string x);
template<> void convertToVariant (UaVariant& variant, OpcUa_UInt32 x);
template<> void convertToVariant (UaVariant& variant, OpcUa_Int32 x);
template<> void convertToVariant (UaVariant& variant, OpcUa_Float x);
template<> void convertToVariant (UaVariant& variant, OpcUa_Double x);

}

template<typename T>
class QuasarFreeVariable
{
public:

	QuasarFreeVariable(
		UaClientSdk::UaSession* session,
        UaNodeId                variableObjId
    ) :
    	m_variantReaderWriter (session, variableObjId)
{}

  T read()
  {
    UaVariant variant ( m_variantReaderWriter.read() );
    return VariantConverters::convertFromVariant<T>(variant);
  }

  void write(T x)
  {
	  UaVariant variant;
	  VariantConverters::convertToVariant(variant, x);
	  m_variantReaderWriter.write(variant);
  }

private:
  VariantReaderWriter m_variantReaderWriter;


};

}
