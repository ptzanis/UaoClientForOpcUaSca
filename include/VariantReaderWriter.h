/*
 * VariantReaderWriter.h
 *
 *  Created on: Sep 7, 2020
 *      Author: pnikiel
 */

#ifndef INCLUDE_VARIANTREADERWRITER_H_
#define INCLUDE_VARIANTREADERWRITER_H_

#include <uaclient/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

class VariantReaderWriter
{
public:
	VariantReaderWriter(
			UaClientSdk::UaSession* session,
			const UaNodeId&         variableNodeId);

	UaVariant read(
			    UaStatus*   out_status = nullptr,
			    UaDateTime* sourceTimeStamp = nullptr,
			    UaDateTime* serverTimeStamp = nullptr);

	void write(
			    UaVariant&     data,
			    UaStatus*      out_status = nullptr);

private:
	UaClientSdk::UaSession* m_session;
	UaNodeId                m_variableNodeId;
};

}

#endif /* INCLUDE_VARIANTREADERWRITER_H_ */
